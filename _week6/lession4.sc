// Maps
val capitals = Map("US" -> "Washington", "RU" -> "Moscow")

// capitals("CN") // returns an NoSuchElementException

capitals get "RU"
capitals get "CN"
// Some is case subclass for an Option
// None is companion object for an Option

val fruits = List("apple", "pear", "orange", "pineapple")

// sorting
fruits sortWith (_.length < _.length)
fruits sorted // by natural order

// grouping
fruits groupBy (_.head)
fruits groupBy (_.tail)
fruits groupBy (_.length)

// Exercise
class Polynomial(val terms : Map[Int, Double]) {
  def + (other: Polynomial) = {

  }
}

val p1 = new Polynomial(Map(1 -> 2.0, 3 -> 4.0, 5 -> 6.2))
val p2 = new Polynomial(Map(0 -> 3.0, 3 -> 7.0))

p1 + p2
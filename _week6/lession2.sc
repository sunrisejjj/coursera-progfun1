// calculate all pairs for 1..n, 1..m which sum is prime

def isPrime(n: Int): Boolean = !(2 until n exists(n % _ == 0))

val n = 7
val m = 11
// first try
(1 until n) map (i =>
    (1 until i) map (j => (i,j))) flatten

// second try
(1 until n) flatMap (i =>
    (1 until i) map (j => (i,j)))

// third try: For expression
for (
    i <- 1 until m;
    j <- 1 until i
    if isPrime(i + j)
) yield (i,j)

for {
    i <- 1 to n if i % 2 == 0
    j <- 1 to m if j % 2 == 0
} yield (i,j)

// Exercise
def scalarProduct(xs: List[Double], ys: List[Double]): Double = {
    (for ((x, y) <- xs zip ys ) yield x * y) sum
}

scalarProduct(List(1, 2, 3), List(4, 5, 6))

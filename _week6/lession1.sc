val xs = Array(1, 2, 3, 44)
xs map (x => x * 2)

val s = "Hello World"
s filter (_.isUpper)
s filterNot (_.isUpper)

// Ranges
val r1: Range = 1 to 5
val r2: Range = 1 until 5

1 to 5
1 until 5
1 to 10 by 3
6 to 1 by -1

// additional operations
s exists (_.isUpper)
s forall (_.isUpper)

(1 to s.length) zip s
(1 to s.length) zip s unzip

s map (List('.', _)) flatten

s flatMap (List('.', _))

// Exercise #1
val N = 3
val M = 5
// todo wtf
(1 to M) flatMap (x => (1 to N) map (y => (x, y)))

// Exercise #2
def scalarProduct(xs: Vector[Double], ys: Vector[Double]) = {
  // (xs zip ys) map (pair => pair._1 * pair._2) sum
  (xs zip ys) map { case (x, y) => x * y } sum
}

scalarProduct(Vector(1, 2, 3), Vector(4, 5, 6))

// Exercise #3
def isPrime(n: Int): Boolean = {
  !(2 until n exists(n % _ == 0)) // my try
  /* Coursera try
  (2 until n) forall (d => u % d != 0)
   */
}

isPrime(1)
isPrime(2)
isPrime(3)
isPrime(4)
isPrime(9)
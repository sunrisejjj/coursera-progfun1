package recfun

import scala.annotation.tailrec

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 20) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

    println()


    println("Balance")
    var variant = ""
    if (balance(variant.toList)) println(variant + " is balanced") else println(variant + " is NOT balanced")
    variant = ":-)"
    if (balance(variant.toList)) println(variant + " is balanced") else println(variant + " is NOT balanced")
    variant = "(ba()la(nc())ed)"
    if (balance(variant.toList)) println(variant + " is balanced") else println(variant + " is NOT balanced")
    variant = "())("
    if (balance(variant.toList)) println(variant + " is balanced") else println(variant + " is NOT balanced")

    println()

    println("Denomination")
    println(countChange(5, List(1, 2, 3)))
    println(countChange(4, List(1, 2)))

  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c < 0 || r < 0) throw new IllegalArgumentException("Column and row must be positive")

    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    @tailrec
    def loop(chars: List[Char], opened: Int): Boolean = {
      if (chars.isEmpty) true
      else {
        if (chars.head == '(') {
          loop(chars.tail, opened + 1)
        } else if (chars.head == ')') {
          if (opened == 0) false
          else loop(chars.tail, opened - 1)
        } else {
          loop(chars.tail, opened)
        }
      }
    }

    loop(chars, 0)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {

    @tailrec
    def loop(money: Int, coins: List[Int], variants: Int): Int = {
      // cannot create denomination as there no money left or values of all coins higher than money amount
      if (money == 0 || coins.isEmpty) return variants
      // cannot create denomination as there are less money than minimal coin
      if (money < coins.min) return variants

      val maxCoin = coins.max

      // if coins array contains current remainder then consider as additional variant
      var newVariants = variants
      if ((money - maxCoin == 0)
        || (money - maxCoin <= maxCoin) && coins.contains(money - maxCoin)) newVariants = newVariants + 1

      var newCoins = coins
      if (maxCoin >= (money - maxCoin)) {
        newCoins = coins.filter(_ < maxCoin)
      }

      loop(money - maxCoin, newCoins, newVariants)
    }

    if (coins.isEmpty) return 0
    if (money == 0) return 0

    var newCoins = coins.distinct
    var variants = 0
    do {
      variants += loop(money, newCoins, 0)
      newCoins = coins.filter(_ < newCoins.max)
    } while (newCoins.nonEmpty)

    variants


  }

}

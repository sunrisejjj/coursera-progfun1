class Rational(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  private def g = gcd(x, y)

  def numer = x / g

  def denom = y / g

  def neg() = new Rational(-numer, denom)

  def add(that: Rational) = new Rational(numer * that.denom + that.numer * denom, denom * that.denom)

  // def subtract(that: Rational) = new Rational(numer * that.denom - that.numer * denom, denom * that.denom)
  def subtract(that: Rational) = this.add(that.neg)

  def multiply(that: Rational) = new Rational(numer * that.numer, denom * that.denom)

  def divide(that: Rational) = new Rational(numer * that.denom, denom * that.numer)

  override def toString(): String = x + "/" + y

}

val test = new Rational(1, 2)
println(test)
println(test.neg)
println(test.add(new Rational(1, 3)))
println(test.subtract(new Rational(1, 3)))
println(test.multiply(new Rational(1, 3)))
println(test.divide(new Rational(1, 3)))

// course question
val a = new Rational(1, 3)
val b = new Rational(5, 7)
val c = new Rational(3, 2)
println(a + " minus " + b + " minus " + c + " is: " + (a.subtract(b).subtract(c)))


// lession 2.6
println(new Rational(2, 10))
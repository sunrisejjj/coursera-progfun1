def sumTail(f: Int => Int, a: Int, b: Int): Int = {
  def loop(a: Int, acc: Int): Int = {
    if (a > b) acc
    else loop(a + 1, acc + f(a))
  }

  loop(a, 0)
}

// example 1
// function which retunts another function
def sum(f: Int => Int): (Int, Int) => Int = {
  // sumF is especially (Int, Int) => Int function
  def sumF(a: Int, b: Int): Int = {
    if (a > b) 0
    else f(a) + sumF(a + 1, b)
  }
  sumF
}

def sunInts = sum(x => x)
def sumCubes = sum(x => x * x * x)

println(sunInts(1, 5) + sumCubes(5, 10))

// example 2
// getting rid of middleman functions
def id(x: Int): Int = x
def cube(x: Int): Int = x * x * x

// sum (cube) applies function 'sum' to 'cube' argument
// sum(cube) is the same as sumCubes
// then '(Int, Int) => Int' function returned by 'sum(cube)' applied to (1, 10) arguments
sum(cube)(1, 10)

// example 3
// multiple parameter list
// 'def sum(f: Int => Int): (Int, Int) => Int' is the same as
def sumMultipleParameterList(f: Int => Int)(a: Int, b: Int): Int =
  if (a > b) 0 else f(a) + sum(f)(a + 1, b)

println("Currying test:")
def test1(a: Int)(b: Int)(c: Int): Int = {
  a * b * c
}
def test2 = {
  a: Int => {
    b: Int => {
      c: Int => {
        a * b * c
      }
    }
  }
}

println("test1")
//println(test1)
//println(test1(1))
//println(test1(1)(2))
println(test1(1)(2)(3))

println("test2")
println(test2)
println(test2(1))
println(test2(1)(2))
println(test2(1)(2)(3))

// exercise 1
def productOld(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 1
  else f(a) * productOld(f)(a + 1, b)
}

// generalization of functions 'sum' and 'productOld'
def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unitValue: Int)(a: Int, b: Int): Int = {
  if (a > b) unitValue
  else combine(f(a), mapReduce(f, combine, unitValue)(a + 1, b))
}

def product(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b)
def sumNew(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x + y, 0)(a, b)
// sum in a new manner
println(sumNew(x => x)(1, 10))

// factorial in a new manner
def factorial(n: Int) = product(x => x)(1, n)
println(factorial(5))

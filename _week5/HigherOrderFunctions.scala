val data = List("pen", "pineapple", "apple", "penpineappleapplepen")

// Map (apply f to every element of data structure)
println("map:       " + data.map(x => x + "_mapped") )

// Filter (retursn elements matches given predicate)
println("filter:    " + data.filter(x => x.length == 3) )

// Other useful stuff
println("filterNot: " + data.filterNot(x => x.length == 3) )
println("partition: " + data.partition(x => x.length > 5) )

println("takeWhile: " + data.takeWhile(x => x.length < 5) )
println("dropWhile: " + data.dropWhile(x => x.length < 5) )
println("span:      " + data.span(x => x.length < 5) )
import math.Ordering

// Merge sort: try 3
def msort[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
	val n = xs.length / 2;
	if (n == 0) xs
	else {
		def merge(left: List[T], right: List[T]): List[T] = (left, right) match {
			case (Nil, l2) => l2
			case (l1, Nil) => l1
			case (h1 :: t1, h2 :: t2) => 
				if (ord.lt(h1, h2)) h1 :: merge(t1, right)
				else h2 :: merge(left, t2)
		}
		val (left, right) = xs splitAt n
		merge(msort(left), msort(right)) // implicit argument 'ord' visible at that point
	}
}

val nums = List(1, -5, 4, 8, 16)
val fruits = List("apple", "banana", "pineapple")

// previous version without Implicit
println( msort(nums)(Ordering.Int) )
println( msort(fruits)(Ordering.String) )

// new version WITH implicits
println( msort(nums) ) // implicit Ordering.Int defined in Ordering companion object
println( msort(fruits) ) // implicit Ordering.String defined in Ordering companion object

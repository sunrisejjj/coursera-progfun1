// Pairs and tuples
// pairs
val pair = ("answer", 42)
println(pair)
println("pair left part is " + pair._1)
println("pair right part is " + pair._2)

// pattern matching
pair match {
	case (label, value) => println(label + " = " + value)
	case _ => println("whatever")
}

// tuples
val triple = ("a", "b", "c")
println(triple._1)
println(triple._2)
println(triple._3)


// splitAt usage example
val l1 = List(1, 2, 3, 4)
println(l1 splitAt 2)

val l2 = l1 ::: List(5)
println(l2 splitAt 2)


// Merge sort: try 1
def msort1(xs: List[Int]): List[Int] = {
	val n = xs.length / 2;
	if (n == 0) xs
	else {
		def merge(left: List[Int], right: List[Int]): List[Int] = {
			left match {
				case Nil => right
				case h1 :: t1 => right match {
					case Nil => left
					case h2 :: t2 =>
						if (h1 < h2) h1 :: merge(t1, right)
						else h2 :: merge(left, t2)
				}
			}
		}
		val (left, right) = xs splitAt n
		merge(msort1(left), msort1(right))
	}
}


// Merge sort: try 2
def msort2(xs: List[Int]): List[Int] = {
	val n = xs.length / 2;
	if (n == 0) xs
	else {
		def merge(left: List[Int], right: List[Int]): List[Int] = (left, right) match {
			case (Nil, l2) => l2
			case (l1, Nil) => l1
			case (h1 :: t1, h2 :: t2) => 
				if (h1 < h2) h1 :: merge(t1, right)
				else h2 :: merge(left, t2)
		}
		val (left, right) = xs splitAt n
		merge(msort2(left), msort2(right))
	}
}

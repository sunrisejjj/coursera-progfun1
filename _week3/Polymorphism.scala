trait List[t] {

	def isEmpty: Boolean
	def head: T
	def tail: List[T]

}

class Nil[T] extends List[T] {

	def isEmpty: Boolean = true
	def head: Nothing = throw NoSuchElementException("Nil.head")
	def tail: Nothing = throw NoSuchElementException("Nil.tail")

}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {

	def isEmpty: Boolean = false
	// def head is implicitly 'implmented' by class value parameter 'head'
	// def tail is implicitly 'implmented' by class value parameter 'tail'

}

/*
object List {

	def List(): List := new Nil()
	def List(n: Int): List = new Cons(n, List())
	def List(n: Int, m: Int): List = ???

}
*/

// Exersize:
// my try
def nth(n: Int, list: List[T]): T = {

	def loop(curr: Int, currList: List[T]) {
		if (curr == n) currList.head
		else loop(curr + 1, currList.tail)
	}

	if (n < 0) throw new IndexOutOfBoundsException("n cannot be negative")
	loop(0, list)
}
// Martin Odersky try { 
  def nth(n: Int, list: List[T]): T = {
	if (list.isEmpty) throw new IndexOutOfBoundsException
	if (n == 0) list.head
	else nth(n - 1, list.tail)
}

## :: Introduction :: ##
Project for storing sources for Coursera progfun1
(https://www.coursera.org/learn/progfun1/home) course.

## Project structure: ##
* Folders _week1 to _week6 contains sources written ax examples specific lessions
* All assignments contains in src/main/scala directory
* Week 1 - Functions & Evaluation - _recfun_ folder
* Week 2 - Higher Order Functions - _funsets_ folder
* Week 3 - Data and Abstraction - _objsets_ folder
* [Not done yet] Week 4 - Data and Abstraction - _patman_ folder
* [Not done yet] Week 5 - Lists - no assignment
* [Not done yet] Week 6 - Collections - _forcomp_ folder
* Tests for all assignments contains in src/test/scala directory
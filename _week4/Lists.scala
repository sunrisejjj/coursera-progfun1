/*
	Insertion sorting:
	nums = List(a, b, c)
	1) Sort tail of nums
	2) Insert head of nums to right place
*/
def isort(xs: List[Int]): List[Int] = xs match {
	case List() => List()
	case y :: ys => insert(y, isort(ys))
}

def insert(x: Int, xs: List[Int]): List[Int] = xs match {
	case List() => List(x)
	case y :: ys => if (x <= y) x :: xs else y :: insert(x, ys)
}

val nums = List(2, 6, 9, 3, 5)
println(isort(nums))
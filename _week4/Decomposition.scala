// interpreter for aithmetic expressions
// restricted to numbers and additions

// solution 1:
trait Expr {

	// classification methods
	def isNumber: Boolean
	def isSum: Boolean

	// accessors
	def numValue: Int
	def leftOp: Expr // left operand
	def rightOp: Expr // right operand

}

class Number(n: Int) extends Expr {

	def isNumber: Boolean = true
	def isSum: Boolean = false
	def numValue: Int = n
	def leftOp: Expr = throw new Error("Number.leftOp")
	def rightOp: Expr = throw new Error("Number.rightOp")

}

class Sum(e1: Expr, e2: Expr) extends Expr {

	def isNumber: Boolean = false
	def isSum: Boolean = true
	def numValue: Int = throw new Error("Sum.numValue")
	def leftOp: Expr = e1 // left operand
	def rightOp: Expr = e2 // right operand

}

def eval(e: Expr): Int = {
	if (e.isNumber) e.numValue
	else if (e.isSum) eval(e.leftOp) + eval(e.rightOp)
	else throw new Error("Unknown expression: " + e)
}

// 'hacky' solution: type testing
// +: no classification methods
// -: unsafe; huge EVAL method
def eval_typecasts(e: Expr): Int = {
	if (e.isInstanceOf[Number])
		e.asInstanceOf[Number].numValue
	else if (e.isInstanceOf[Sum])
		eval(e.asInstanceOf[Sum].leftOp) + 
		eval(e.asInstanceOf[Sum].rightOp)
	else throw new Error("Unknown expression: " + e)
}

// solution 2:
// +: more clear and object-oriented
// -: addition of new methods (like def show: String) forces update of EVERY subclass
// -: not possible to perform non-local operations (like simplification)
trait Expr2 {
	def eval: Int
}

class Number2(n: Int) = {
	def eval: Int = n
}

class Sum2(e1: Expr, e2: Expr) = {
	def eval: Int = e1.eval + e2.eval
}
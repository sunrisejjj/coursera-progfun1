/*
	Task: general way to access objects in a extensible class hierarchy
	Try 1: classification and access methods: quadratic explosion
	Try 2: type checks and casting: unsafe
	Try 3: object-oriendted decomposition: high-level changes affects all subclasses; doesn't work for non-local operations
*/

trait Expr {

	def eval: Int = this match {
		case Number(n) => n
		case Sum(e1, e2) => e1.eval + e2.eval
	}

}

def show(e: Expr): String = e match {
	case Number(n) => n.toString
	case Sum(l, r) => show(l) + " + " + show(r)
}

case class Number(n: Int) extends Expr {}
/*
	implicitly adds
	object Number {
		def apply(n: Int) = new Number(n)
	}
*/

case class Sum(e1: Expr, e2: Expr) extends Expr {}
/*
	implicitly adds
	object Sum {
		def apply(e1: Expr, e2: Expr) = new Sum(e1, e2)
	}
*/

println(show( Sum(Number(1), Sum(Number(2), Number(42))) ))
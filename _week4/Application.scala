import java.util._

trait List[T] {

	def isEmpty: Boolean
	def head: T
	def tail: List[T]

	// Exersize:
	// my try
	def nth1(n: Int, list: List[T]): T = {

		def loop(curr: Int, currList: List[T]): T = {
			if (curr == n) currList.head
			else loop(curr + 1, currList.tail)
		}

		if (n < 0) throw new IndexOutOfBoundsException("n cannot be negative")
		loop(0, list)
	}
	// Martin Odersky try
	def nth2(n: Int, list: List[T]): T = {
		if (list.isEmpty) throw new IndexOutOfBoundsException
		if (n == 0) list.head
		else nth2(n - 1, list.tail)
	}

}

class Nil[T] extends List[T] {

	def isEmpty: Boolean = true
	def head: Nothing = throw new NoSuchElementException("Nil.head")
	def tail: Nothing = throw new NoSuchElementException("Nil.tail")

}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {

	def isEmpty: Boolean = false
	// def head is implicitly 'implmented' by class value parameter 'head'
	// def tail is implicitly 'implmented' by class value parameter 'tail'

}

// 4.2 Application exersize
object List {

	def apply[T](): List[T] = new Nil
	def apply[T](n: T): List[T] = new Cons(n, List())
	def apply[T](n: T, m: T): List[T] = new Cons(n, List(m))

}

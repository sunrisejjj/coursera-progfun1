def factorial(n: Int, current: Int, accumulator: Long): Long = {
  if (current <= n) factorial(n, current + 1, accumulator * current)
  else accumulator
}

def factorial2(n: Int): Long = {
  def loop(acc: Long, n: Int): Long {
    if (n == 0) acc
    else loop (acc * n, n - 1)
  }
}
import Math._

def sqrt(x: Double, measure: Double): Double = {
  if (x < 0) throw new IllegalArgumentException("X should be positive")

  def sqrtIter(estimate: Double, measure: Double): Double = {
    def result = 0.5 * (estimate + x / estimate)
    if (goodEnough(result, measure)) result else sqrtIter(result, measure)
  }

  def goodEnough(current: Double, measure: Double): Boolean = {
    abs(current * current - x) / x <= measure
  }

  sqrtIter(1, measure)
}

sqrt(2, 0.001)
sqrt(4, 0.001)
sqrt(1e-6, 0.001)
sqrt(1e60, 0.001)